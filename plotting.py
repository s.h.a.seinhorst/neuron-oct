#!/usr/bin/env python
import path_tracing as ptr
import oct_neuron as neuron
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, colors
import warnings
from scipy.optimize import curve_fit
from scipy.ndimage.filters import uniform_filter1d
from multiprocessing import Pool
from functools import partial
pi = np.pi
np.set_printoptions(linewidth=320, precision=4)


def plotFocusDistance(rayCount=3000):
    wavenumbers = neuron.genWavenumbers()
    lensZs = np.linspace(-0.5, 3.5, 151)
    detectedRayCount = np.empty_like(lensZs, dtype=int)
    detectedAmplitude = np.empty_like(lensZs)
    detectedIntensity = np.empty_like(lensZs)
    for i, lensZ in enumerate(lensZs):
        scene, ed = neuron.OCTNeuronScene(diam=2.0, focusZ=lensZ)
        rays = ed.generateRays(rayCount)
        rays.trace(scene, ed, maxBounces=11)

        detectedRayCount [i] = ed.ampls.size
        detectedAmplitude[i] = abs(ed.ampls).sum()
        detectedIntensity[i] = ptr.complexNormSq(ed.readAmplitudeSignal(wavenumbers)).sum()

    fig, axl = plt.subplots(figsize=(4.8,3.0), constrained_layout=True)
    axr = axl.twinx()

    color="tab:blue"
    a, = axl.plot(lensZs, np.log10(detectedRayCount), label="Ray count", c=color)
    axl.tick_params(axis="y", color=color, labelcolor=color)
    # axl.set_ylim(0,3.2)
    axl.set_xlim(lensZs[0], lensZs[-1])

    color="tab:red"
    f, = axr.plot(lensZs, np.log10(detectedIntensity), label="Total intensity", c=color)
    axr.tick_params(axis="y", color=color, labelcolor=color)
    axr.set_xlim(lensZs[0], lensZs[-1])

    axl.legend(p := [a,f], [p_.get_label() for p_ in p])
    axl.set_xlabel(r"Focus position $z$ (µm)")
    axl.set_ylabel(r"$\log_{10}$(Ray count)")
    axr.set_ylabel(r"$\log_{10}$(Intensity)")
    return fig


def plotPhaseSlopeOverFocus(rayCount=3000):
    wavenumbers = neuron.genWavenumbers()
    lensZs = np.linspace(0.5, 2.0, 151)
    diameters = np.linspace(2.000, 2.020, 31)
    detectedRayCount = np.empty_like(lensZs, dtype=int)
    detectedIntensity = np.empty_like(lensZs)
    meanSlopes = np.empty_like(lensZs)
    phases = np.empty_like(diameters)
    peakIdcs = np.empty_like(diameters, dtype=int)
    for i, lensZ in enumerate(lensZs):
        meanSlopes[i] = neuron.processSignalSeries(
                *neuron.spectraScan(
                    neuron.OCTNeuronScene,
                    lambda _: lensZ,
                    diameters=diameters,
                    wavenumbers=wavenumbers)
                ).mean()

        scene, ed = neuron.OCTNeuronScene(diam=diameters[0], focusZ=lensZ)
        rays = ed.generateRays(rayCount)
        rays.trace(scene, ed, maxBounces=11)
        detectedRayCount[i] = ed.ampls.size
        detectedIntensity[i] = ptr.complexNormSq(ed.readAmplitudeSignal(wavenumbers)).sum()

    fig = plt.figure(constrained_layout=True)
    plt.plot(lensZs, meanSlopes, label="FFT phase slope (mrad/nm)")
    plt.axhline(scene[1].iorInside*(wavenumbers[0] + wavenumbers[-1]), label="Expected phase slope", c='k', ls='--')
    plt.plot(lensZs, np.log10(detectedIntensity), label=r"$\log_{10}$(intensity)")
    plt.xlabel(r"Focus position $z$ (µm)")
    plt.legend()
    plt.xlim(lensZs[0], lensZs[-1])

    return fig


def plotOptimalFocus(rayCount=3000):
    def gaussian(x, offset, amplitude, mean, sigma):
        return offset + amplitude * np.exp(-(x-mean)**2 / (2 * sigma**2))

    wavenumbers = neuron.genWavenumbers()
    diameters = np.linspace(1.0, 2.0, 11)
    optimalFocus = np.empty_like(diameters)
    fig = plt.figure(figsize=(4.8,3.0), constrained_layout=True)
    for j, diam in enumerate(diameters):
        lensZs = np.linspace(0.4*diam, 0.8*diam, 11)  # Does not work for larger diameters
        detectedIntensity = np.empty_like(lensZs)
        for i, lensZ in enumerate(lensZs):
            detectedIntensity[i] = neuron.renderSpectrum(*neuron.OCTNeuronScene(diam, lensZ), wavenumbers, rayCount).sum()

        guess_mean = np.sum(lensZs * detectedIntensity) / np.sum(detectedIntensity)
        guess_sigma = np.sqrt(np.sum(detectedIntensity * (lensZs - guess_mean)**2) / np.sum(detectedIntensity))
        popt, _ = curve_fit(gaussian, lensZs, detectedIntensity, p0=[detectedIntensity.min(), detectedIntensity.max(), guess_mean, guess_sigma])
        optimalFocus[j] = popt[2]
        if not (lensZs[0] < optimalFocus[j] < lensZs[-1]):
            warnings.warn("Intensity peak was not in the window where it was expected")

    plt.plot(diameters, optimalFocus, label="Optimal focus")
    plt.xlabel(r"Cylinder diameter (µm)")
    plt.ylabel(r"Optimal focus position (µm)")
    plt.xlim(diameters[0], diameters[-1])

    b, a = np.polyfit(diameters, optimalFocus, 1)
    plt.plot(diameters, b * diameters + a, '--k', label=f"$z={b:.3f}d+{a:.3f}$", alpha=0.4)
    plt.legend()

    return fig


def plotPhaseSlopeOverDiameter(**kwargs):
    wavenumbers = neuron.genWavenumbers()
    diameters = np.linspace(1.0, 2.0, 151)

    slopeKwargs = {
        "wavenumbers": wavenumbers,
        "sceneFunc": neuron.OCTNeuronScene,
        "focusFunc": neuron.optimalFocus,
        **kwargs
    }
    diams = np.linspace(diameters, diameters+0.020, 16, axis=-1)
    with Pool() as p:
        meanSlopes = p.map(partial(neuron.phaseSlope, **slopeKwargs), diams)

    cylIOR = slopeKwargs['sceneFunc'](1,1)[0][1].iorInside

    fig = plt.figure(constrained_layout=True)
    ax = fig.gca()
    ax.plot(diameters, meanSlopes, label="FFT phase slope")
    ax.axhline(cylIOR*(wavenumbers[0] + wavenumbers[-1]), label="Expected phase slope", c='k', ls='--')
    ax.set_xlabel(r"Cylinder diameter (µm)")
    ax.set_ylabel("Phase slope (mrad/nm)")
    ax.legend()
    ax.set_xlim(diameters[0], diameters[-1])

    return fig


def plot3DRays(bounces='ttrtt', rayCount=30):
    scene, ed = neuron.OCTNeuronScene(diam=2.0, focusZ=neuron.optimalFocus(2.0))

    def reflection(rays):
        dists, newLocs, normals, iors, rays, _ = rays.nextIntersect(scene, ed)
        reflDirs, _, _, _ = rays.snellFresnel(normals, iors)
        rays.locs, rays.dirs = newLocs, reflDirs
        return rays, dists

    def transmission(rays):
        dists, newLocs, normals, iors, rays, _ = rays.nextIntersect(scene, ed)
        _, transDirs, _, _ = rays.snellFresnel(normals, iors)
        rays.locs, rays.dirs = newLocs, transDirs
        return rays, dists

    class PlotData():
        def __init__(self):
            self.locs = np.empty((3,0))
            self.dirs = np.empty((3,0))
            self.dists = np.empty((0,))

        def add(self, rays, dists):
           self.locs = np.hstack((self.locs, rays.locs))
           self.dirs = np.hstack((self.dirs, rays.dirs))
           self.dists = np.hstack((self.dists, dists))


    fig = plt.figure(figsize=(5.0,3.0), constrained_layout=True)
    ax = fig.add_subplot(111, projection='3d')
    ax.set_box_aspect((1,1,1))
    ax.view_init(0,0)
    # ax.axis('off')
    ax.set_zlabel(r"z (µm)")
    ax.set_xlabel(r"x (µm)")
    ax.set_ylabel(r"y (µm)")
    ax.set_xlim(-1.5,1.5)
    ax.set_ylim(-1.5,1.5)
    ax.set_zlim(-1,2)

    # Plot a cylinder and a plane
    [plane, cyl] = scene
    a=np.linspace(0, 2*pi, 30)
    x=np.linspace(*ax.get_xlim(), 2)
    Ac, Xc=np.meshgrid(a, x)
    Yc = np.cos(Ac) * cyl.r + cyl.loc[0]
    Zc = np.sin(Ac) * cyl.r + cyl.loc[1]
    ax.plot_surface(Xc, Yc, Zc, alpha=0.2, color='k')
    ax.plot_surface(np.array([ax.get_xlim(),ax.get_xlim()]).T,
                    np.array([ax.get_ylim(),ax.get_ylim()])*10,
                    np.array([[plane.z,plane.z],[plane.z,plane.z]]),
                    alpha=0.2, color='k')

    # Rays coming from ed are 300 um long which mpl does not like, so plot only the last 1 um
    rays = ed.generateRays(rayCount)
    rayToPlaneLength = plane.raysIntersect(rays)
    closerRayLocs = rays.locs + rays.dirs * (rayToPlaneLength - 1.0)
    ax.quiver(*closerRayLocs, *rays.dirs, length=1.0, alpha=0.5, color="tab:blue", arrow_length_ratio=0.0)

    if not bounces:
        return fig

    # The rest of the rays all differ in length. We must plot each ray individually
    plotData = PlotData()

    # Trace the rays
    ## The incoming rays were rendered before
    bounces = bounces.lower()
    bounce, bounces = bounces[0], bounces[1:]
    if bounce == 'r':
        rays, _ = reflection(rays)
    elif bounce == 't':
        rays, _ = transmission(rays)
    dists = []

    for bounce in bounces:
        plotData.add(rays, dists)
        if bounce == 'r':
            rays, dists = reflection(rays)
        elif bounce == 't':
            rays, dists = transmission(rays)
        else:
            raise ValueError(f"Unknown letter {bounce}, should be r ot t.")
    plotData.add(rays[:0], dists)  # The outgoing rays will be rendered after

    # Plot each individual ray
    for location, direction, distance in zip(plotData.locs.T, plotData.dirs.T, plotData.dists):
        ax.quiver(*location, *direction, length=distance, alpha=0.5, color="tab:blue", arrow_length_ratio=0.0)

    # The rays exiting the setup are very long, so make them just 1 um
    ax.quiver(*rays.locs, *rays.dirs, length=1.0, alpha=0.5, color="tab:blue", arrow_length_ratio=0.0)

    return fig


def plotReturnedWeakRayLocs(scene, ed, rayCount=3000, maxBounces=5, maxAmplitude=0.01):
    rays = ed.generateRays(rayCount)
    rays = rays.trace(scene, ed, maxBounces=maxBounces)
    weakRays = np.abs(ed.returnedAmpls) < maxAmplitude

    fig = plt.figure(constrained_layout=True)
    plt.scatter(*(ed.returnedLocs[:,weakRays]*-ed.magnification), s=(ed.returnedAmpls[weakRays]*1e5)**2+1, alpha=0.3, lw=0)
    angle = np.linspace(0, 2*pi)
    plt.plot(ed.sensorRadius*np.cos(angle), ed.sensorRadius*np.sin(angle), c="tab:red")
    plt.axis('equal')

    return fig


def plotSpectra(diam=2.0, delta=0.01):
    diameters, diamStep = np.linspace(diam-delta, diam+delta, 21, retstep=True)
    wavenumbers = neuron.genWavenumbers()
    cmap = cm.get_cmap("turbo", diameters.size)
    norm = colors.Normalize(vmin=diameters[0]-diamStep/2, vmax=diameters[-1]+diamStep/2)
    fig = plt.figure(constrained_layout=True)
    for d in diameters:
        spectrum = neuron.renderSpectrum(*neuron.OCTNeuronScene(diam=d, focusZ=neuron.optimalFocus(d)), wavenumbers)
        plt.plot(wavenumbers, spectrum, c=cmap(norm(d)))
    plt.xlabel("Wavenumber (µm$^{-1}$)")
    plt.ylabel("Intensity")
    plt.xlim(wavenumbers[0], wavenumbers[-1])
    cbar = fig.colorbar(cm.ScalarMappable(norm=norm, cmap=cmap), ticks=diameters[::2], label="Cylinder diameter (µm)")
    cbar.ax.set_yticklabels([f"{d:.3f}" for d in diameters[::2]])
    return fig


def plotSignalProcesssingSteps(spectra, wavenumbers, diameters, start=1, diameterIndex=60, save=False):
    # Scale such that the spectrum reaches from -1 to +1
    maxs = spectra.max(axis=-1)[:,np.newaxis]
    mins = spectra.min(axis=-1)[:,np.newaxis]
    spectra = (2 * spectra - (maxs + mins)) / (maxs - mins)

    spectrumNorm = spectra[diameterIndex].copy()
    plt.figure(figsize=(4.8,3.0), constrained_layout=True)
    plt.plot(wavenumbers, spectrumNorm, c='tab:blue')
    plt.xlim(wavenumbers[0], wavenumbers[-1])
    plt.xlabel("Wavenumber (rad/µm)")
    plt.ylabel("Intensity")
    if save: plt.savefig("thesis_spectrum_normed.pdf")
    plt.show()

    spectra *= np.hamming(spectra.shape[1])

    spectrumWindowed = spectra[diameterIndex].copy()
    plt.figure(figsize=(4.8,3.0), constrained_layout=True)
    plt.plot(wavenumbers, spectrumNorm, c='tab:blue', alpha=0.5)
    plt.plot(wavenumbers, spectrumWindowed, c='tab:blue')
    plt.xlim(wavenumbers[0], wavenumbers[-1])
    plt.xlabel("Wavenumber (rad/µm)")
    plt.ylabel("Intensity")
    if save: plt.savefig("thesis_spectrum_windowed.pdf")
    plt.show()

    spectra -= spectra.mean(axis=-1)[:,np.newaxis]

    spectrumFinal = spectra[diameterIndex].copy()
    plt.figure(figsize=(4.8,3.0), constrained_layout=True)
    plt.plot(wavenumbers, spectrumNorm, c='tab:blue', alpha=0.25)
    plt.plot(wavenumbers, spectrumWindowed, c='tab:blue', alpha=0.5)
    plt.plot(wavenumbers, spectrumFinal, c='tab:blue')
    plt.xlim(wavenumbers[0], wavenumbers[-1])
    plt.xlabel("Wavenumber (rad/µm)")
    plt.ylabel("Intensity")
    if save: plt.savefig("thesis_spectrum_final.pdf")
    plt.show()

    fourier = np.fft.rfft(spectra)
    realSpace = np.fft.rfftfreq(wavenumbers.size, (wavenumbers[-1] - wavenumbers[0])/(wavenumbers.size - 1)) * pi
    FFTIntensity = ptr.complexNormSq(fourier)
    peakIndex = np.argmax(FFTIntensity, axis=-1)

    plt.figure(figsize=(4.8,3.0), constrained_layout=True)
    plt.plot(realSpace, FFTIntensity[diameterIndex], label="FFT from signal")
    plt.scatter(realSpace[1], FFTIntensity[diameterIndex,1], c='tab:blue', s=100, zorder=5)
    plt.scatter(realSpace[2], FFTIntensity[diameterIndex,2], c='tab:orange', s=100, zorder=5)
    plt.xlim(0, 5)
    plt.ylim(0, None)
    # plt.axvline(diameters[diameterIndex] * 1.39, c='tab:red', label="Expected peak position")
    # plt.legend()
    plt.xlabel("$z$ (µm)")
    plt.ylabel("FFT$^2$")
    plt.tick_params(left=None, labelleft=None)
    if save: plt.savefig("thesis_fourier_single.pdf")
    plt.show()

    fourierFig = plt.figure(figsize=(4.8,3.0), constrained_layout=True)
    fourierAxis = fourierFig.gca()

    lines=[]

    for i in range(start,8):
        isPeak = peakIndex == i
        line = fourierAxis.plot(diameters[~isPeak], FFTIntensity[~isPeak,i], ls=':')[0]
        lines += fourierAxis.plot(diameters[isPeak], FFTIntensity[isPeak,i], c=line.get_c())

    plt.xlim(diameters[0], diameters[-1])
    plt.ylim(0, None)
    plt.xlabel("Cylinder diameter (µm)")
    plt.ylabel("FFT$^2$")
    plt.tick_params(left=None, labelleft=None)
    plt.legend(lines, map(lambda x: f'{x:.2f} µm', realSpace[1:3]), title="FFT component $z$")
    if save: plt.savefig("thesis_fourier_all.pdf")
    plt.show()

    colors = ["tab:blue", "tab:orange"]
    lines = lines[:len(colors)]
    phases = np.unwrap(2 * np.angle(fourier[:, start:start+len(colors)]), axis=0) / 2  # TODO use 'period = pi' when using NumPy 1.21.0+
    diameterSteps = np.diff(diameters)
    finalSlope = np.empty_like(diameterSteps)

    phaseFig = plt.figure(figsize=(4.8,3.0), constrained_layout=True)
    phaseAxis = phaseFig.gca()

    slopeFig = plt.figure(figsize=(4.8,3.0), constrained_layout=True)
    slopeAxis = slopeFig.gca()
    inBetween = (diameters[:-1] + diameters[1:])/2.0

    finalSlope = np.empty(diameters.size-1)
    for i, c in enumerate(colors):
        isPeak = peakIndex == i + start
        slope = np.diff(phases[:,i])/diameterSteps
        finalSlope[isPeak[:-1]] = slope[isPeak[:-1]]

        nonPeakPhases = phases[:,i].copy()
        nonPeakPhases[isPeak] = np.nan
        nonPeakSlopes = slope.copy()
        nonPeakSlopes[isPeak[:-1]] = np.nan

        phases[~isPeak,i] = np.nan
        phaseAxis.plot(diameters, phases[:,i], c=c)
        phaseAxis.plot(diameters, nonPeakPhases, ':', c=c, alpha=0.5)

        slope[~isPeak[:-1]] = np.nan
        slopeAxis.plot(inBetween, slope, zorder=5)
        slopeAxis.plot(inBetween, nonPeakSlopes, ':', c=c, alpha=0.5)
        finalSlope[isPeak[:-1]] = slope[isPeak[:-1]]

    phaseAxis.set_xlim(diameters[0], diameters[-1])
    phaseAxis.set_xlabel("Cylinder diameter (µm)")
    phaseAxis.set_ylabel("FFT phase (rad)")
    phaseAxis.legend(lines, map(lambda x: f'{x:.2f} µm', realSpace[1:3]), title="FFT component $z$")
    if save: phaseFig.savefig("thesis_phase.pdf")

    iorPrediction = 1.39
    idealSpectra = np.cos(2 * iorPrediction * np.outer(diameters, wavenumbers))
    idealSlope = neuron.processSignalSeries(idealSpectra, wavenumbers, diameters)

    slopeAxis.set_xlim(diameters[0], diameters[-1])
    # slopeAxis.set_ylim(19.4, 30.2)
    pred = slopeAxis.axhline(iorPrediction * (wavenumbers[0] + wavenumbers[-1]), c='k', ls='--', zorder=6)
    specPred = slopeAxis.plot(inBetween, idealSlope.squeeze(), ls='--', c='tab:green')
    slopeAxis.legend([*lines[:3], *specPred, pred], [*map(lambda x: f'{x:.2f} µm', realSpace[1:3]), "Prediction from ideal spectrum", "Prediction from setup"], title="FFT component $z$")
    slopeAxis.set_xlabel("Cylinder diameter (µm)")
    slopeAxis.set_ylabel("FFT phase slope (mrad/nm)")
    if save: slopeFig.savefig("thesis_slope_raw.pdf")
    plt.show()

    print("Standard deviation from setup-based prediction (constant slope):", np.sqrt(((finalSlope - iorPrediction * (wavenumbers[0] + wavenumbers[-1]))**2).sum()/(finalSlope.size-1)))
    print("Standard deviation from setup-based prediction (constant slope), ignoring spike:", np.sqrt(((finalSlope - iorPrediction * (wavenumbers[0] + wavenumbers[-1]))[finalSlope < 24.0]**2).sum()/(finalSlope.size-1)))
    print("Standard deviation from signal-based prediction (oscillating slope):", np.sqrt(((finalSlope - idealSlope)**2).sum()/(finalSlope.size-1)))
    print("Standard deviation from signal-based prediction (oscillating slope), ignoring spike:", np.sqrt(((finalSlope - idealSlope)[finalSlope < 24.0]**2).sum()/(finalSlope.size-1)))

    plt.figure(figsize=(4.8,3.0), constrained_layout=True)
    plt.plot(inBetween, finalSlope, alpha=0.5, c='grey', label="Raw slope")
    plt.plot(inBetween, uniform_filter1d(finalSlope, size=11), c='tab:red', label="Rolling mean over 11 points")
    plt.axhline(iorPrediction * (wavenumbers[0] + wavenumbers[-1]), c='k', ls='--', zorder=6, label="Prediction")
    plt.legend()
    plt.xlim(diameters[0], diameters[-1])
    plt.xlabel("Cylinder diameter (µm)")
    plt.ylabel("FFT phase slope (mrad/nm)")
    if save: plt.savefig("thesis_slope_smooth.pdf")
    plt.show()


def main():
    # plotFocusDistance()  # Slow
    # plotPhaseSlopeOverFocus()  # Slow
    # plotOptimalFocus()
    # plotPhaseSlopeOverDiameter()  # Slow
    # plotSignalProcesssingSteps(*neuron.readSpectra("cylinder_spectra_1_2_500_falloff.h5"))
    # plt.figure(); plt.plot(neuron.processSignalSeries(*neuron.readSpectra("cylinder_spectra_1_2_500_falloff.h5")))
    plot3DRays()
    plotReturnedWeakRayLocs(*neuron.OCTNeuronScene(diam=1.0, focusZ=neuron.optimalFocus(1.0)))
    plotSpectra()
    plt.show()

if __name__ == "__main__":
    main()
