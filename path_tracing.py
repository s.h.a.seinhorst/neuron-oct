#!/usr/bin/env python
import numpy as np
import warnings
pi = np.pi

class Rays:
    """Holds the locations, directions, optical path lengths and amplitudes of many rays"""
    def __init__(self, locations, directions, opticalPathLengths=None, amplitudes=None):
        """Verifies and sets locations, directions and, if provided, the optical path lengths and amplitudes.
        locations and directions must be of shape (3,n) and OPLs and amplitudes (n,)."""
        # Convert to float arrays if necessary
        locations  = np.asfarray(locations)
        directions = np.asfarray(directions)

        if locations.shape != directions.shape:
            raise ValueError(f"Locations and directions are a different shape: {locations.shape} and {directions.shape}")
        if locations.ndim != 2 or locations.shape[0] != 3:
            raise ValueError("Locations and directions are not three lists of numbers")

        # Normalizing directions
        directions /= multinorm(directions)

        self.locs = locations
        self.dirs = directions

        if opticalPathLengths is not None:
            opticalPathLengths = np.asfarray(opticalPathLengths)
            if opticalPathLengths.ndim != 1 or opticalPathLengths.size != locations.shape[1]:
                raise ValueError(f"Optical path lengths must be a list of numbers, the same length as location and directions")
            else:
                self.opls = opticalPathLengths
        else:
            self.opls = np.zeros(locations.shape[1])

        if amplitudes is not None:
            amplitudes = np.asfarray(amplitudes)
            if amplitudes.ndim != 1 or amplitudes.size != locations.shape[1]:
                raise ValueError(f"Amplitudes must be a list of numbers, the same length as location and directions")
            else:
                self.ampls = amplitudes
        else:
            self.ampls = np.ones(locations.shape[1])

    def __getitem__(self, slice):
        "Creates new Rays instance which holds the given slice of the rays"
        return Rays(self.locs[:,slice], self.dirs[:,slice], self.opls[slice], self.ampls[slice])

    def nextIntersect(self, scene, detector):
        """Returns the distance to, location of and normal and iors at the closest
        intersect with an object in the scene, and the rays that intersect an object,
        and the rays that intersect the detector. The IORs and normals of other rays
        are NaN."""
        # Figure out how far the rays are from each object along the ray's direction
        dists = np.asanyarray([obj.raysIntersect(self) for obj in [*scene, detector]])
        # Non-intersecting rays return np.NaN
        noIntersectMask = np.isnan(dists)
        # Find the object that is the closest, but argmin does not like NaN so
        # use infinity instead (nanargmin does not like all-nan slices)
        dists[noIntersectMask] = np.inf
        minDistIdx = dists.argmin(axis=0)
        # Filter detected rays and intersection rays
        intersectsDetectorMask = minDistIdx == len(scene)
        objectIntersectMask = ~(noIntersectMask.all(axis=0) | intersectsDetectorMask)
        detectedRays = self[intersectsDetectorMask]
        objectRays = self[objectIntersectMask]
        minDistIdx = minDistIdx[objectIntersectMask]
        # Figure out the location of the intersection
        dists = dists[minDistIdx, np.nonzero(objectIntersectMask)].squeeze()
        intersectLocations = objectRays.locs + objectRays.dirs * dists
        # Calculate the normal vectors
        normals = np.empty_like(intersectLocations)
        iors = np.empty((2,normals.shape[1]))
        for i, obj in enumerate(scene):
            raysMask = minDistIdx == i
            normals[:,raysMask], iors[:,raysMask] = obj.normals(intersectLocations[:,raysMask], objectRays.dirs[:,raysMask])
        return dists, intersectLocations, normals, iors, objectRays, detectedRays

    def snellFresnel(self, normals, iors):
        """Calculate the directions of the reflected and transmitted rays with Snell's law
        and their amplitude coefficients using modified Fresnel equations"""
        iorRat = iors[0]/iors[1]
        # Cosine of negative incoming direction with normal
        cosIn = -multidot(self.dirs, normals)
        # Cosine of transmitted direction with negative normal
        # Total internal reflection results in a negative value under the square root.
        # That's fine. It will be corrected later
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            cosTr = np.sqrt(1.0-iorRat*iorRat*(1.0-cosIn*cosIn))

        # Calculate reflection and transmission directions
        reflDirs = self.dirs + 2.0 * cosIn * normals
        transPara = -cosTr * normals
        transPerp = iorRat * multicross(normals, multicross(self.dirs, normals))
        transDirs = transPara + transPerp

        # Calculate amplitude coefficients
        # split into smaller equations which can be reused
        iorInCosIn = iors[0] * cosIn
        iorInCosTr = iors[0] * cosTr
        iorTrCosIn = iors[1] * cosIn
        iorTrCosTr = iors[1] * cosTr
        sameSum = iorInCosIn + iorTrCosTr
        diffSum = iorTrCosIn + iorInCosTr
        perpReflAmplCoeff = (iorInCosIn - iorTrCosTr) / sameSum
        paraReflAmplCoeff = (iorTrCosIn - iorInCosTr) / diffSum
        twoIorInCosIn = 2.0 * iorInCosIn
        perpTransAmplCoeff = twoIorInCosIn / sameSum
        paraTransAmplCoeff = twoIorInCosIn / diffSum
        reflAmplCoeff  = np.sqrt(0.5 * (perpReflAmplCoeff *perpReflAmplCoeff  + paraReflAmplCoeff *paraReflAmplCoeff ))
        transAmplCoeff = np.sqrt(0.5 * (perpTransAmplCoeff*perpTransAmplCoeff + paraTransAmplCoeff*paraTransAmplCoeff))
        reflAmplCoeff[np.isnan(reflAmplCoeff)] = 1.0  # Total internal reflection
        reflAmplCoeff *= np.sign(iorRat - 1.0)  # Amplitude should invert on external reflections

        return reflDirs, transDirs, reflAmplCoeff, transAmplCoeff

    def trace(self, scene, detector, maxBounces=11):
        """Trace rays for a maximum of maxBounces through the scene and onto the detector.
        Returns a copy of the instance it was called on that can be used to continue ray tracing
        with a new call to this function."""
        bounceIndex = 0
        while bounceIndex <= maxBounces:
            bounceIndex += 1
            # Writing to self affects only local self, not the calling code
            dists, newLocs, normals, iors, self, detectedRays = self.nextIntersect(scene, detector)
            detector.detect(detectedRays)
            reflDirs, transDirs, reflAmplCoeffs, transAmplCoeffs = self.snellFresnel(normals, iors)
            self.locs = np.hstack((newLocs, newLocs))
            self.dirs = np.hstack((reflDirs, transDirs))
            self.opls = self.opls + dists*iors[0]
            self.opls = np.hstack((self.opls, self.opls))
            self.ampls = np.hstack((self.ampls*reflAmplCoeffs, self.ampls*transAmplCoeffs))
        return self


def multidot(a, b):
    "Column-wise dot product of two 2D arrays"
    return np.sum(a * b, axis=0)


def multicross(a, b):
    "Column-wise cross product of two 2D arrays"
    return np.cross(a, b, axis=0)


def multinormSq(a):
    "Column-wise norm-squared of a 2D array"
    return multidot(a, a)


def multinorm(a):
    "Column-wise norm of a 2D array"
    return np.sqrt(multinormSq(a))


def complexNormSq(z):
    "Norm-squared of one or more complex numbers"
    return np.real(z * np.conj(z))


def complexNorm(z):
    "Norm of one or more complex numbers"
    return np.sqrt(complexNormSq(z))


class XCylinder:
    """A cylinder that is parallel to the x-axis"""
    def __init__(self, radius, location, iorInside, iorOutside):
        loc = np.asfarray(location)
        """Radius must be greater that zero, location must be 2D array
        and iorInside and iorOutside must both be greater than one."""

        if loc.shape != (2,):
            raise ValueError(f"Location must be shaped (2,) but is {loc.shape}")

        if radius <= 0.0:
            raise ValueError(f"Radius must be greater than zero but is {radius}")

        self.r = float(radius)
        self.loc = loc  # 2D
        self.iorInside = float(iorInside)
        self.iorOutside = float(iorOutside)

        if self.iorInside < 1.0 or self.iorOutside < 1.0:
            raise ValueError("IOR must be 1.0 or greater")

    def raysIntersect(self, rays):
        """Calculates the distance along the direction vector from each ray to the
        cylinder, or NaN if there is no intersection"""
        # Work in a 2D plane that is perpendicular to the cylinder axis
        # Translate location such that ray location is at the origin
        relLocs = self.loc[:,np.newaxis] - rays.locs[1:]
        # The length of the 2d direction vector is not normalised because we are removing the x-component
        lengths2dDirSq = multinormSq(rays.dirs[1:])
        # We start with the distance from the ray (origin) to the middle of the circle, along the direction vector
        distsAlongDir = multidot(relLocs, rays.dirs[1:])
        # Then the distance from here to either of the circle intersections.
        # It does not matter if this results in an NaN (the ray does not intersect with the cylinder)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            widthsHalfCrossSection = np.sqrt(distsAlongDir*distsAlongDir - lengths2dDirSq * (multinormSq(relLocs) - self.r*self.r))
        # Prefer the closer intersection
        dists = distsAlongDir - widthsHalfCrossSection
        # If this intersection is behind the ray, we use the farther intersection
        distsIsNegative = dists <= 1e-10
        longerDists = (distsAlongDir + widthsHalfCrossSection)[distsIsNegative]
        # If this is also behind the ray, return NaN
        longerDists[longerDists <= 1e-10] = np.NaN
        dists[distsIsNegative] = longerDists
        # Correct for the fact that we projected the direction vectors onto the 2D plane
        return dists / lengths2dDirSq

    def normals(self, locations, rayDirs):
        """Returns normal directions on the surface of the cylinder given the origin
        locations of the normals and the directions of the rays that created them."""
        # The normals point into or out from the circle centre and have no x-component
        selfLoc3d = np.array([[0.0, *self.loc]]).T
        relLocs = locations - selfLoc3d
        relLocs[0] = 0.0
        # Normalize the vectors. Normals point roughly antiparallel to ray directions (negative dot product)
        relLocLengths = multinorm(relLocs)
        flipIfInternal = -np.sign(multidot(rayDirs, relLocs))
        normals = relLocs / (flipIfInternal * relLocLengths)

        # First row is incoming side's IOR, second is the IOR on the other side
        inToOut = flipIfInternal < 0.0
        iors = np.full((2, inToOut.size), self.iorOutside)
        iors[0,  inToOut] = self.iorInside
        iors[1, ~inToOut] = self.iorInside
        return normals, iors


class XYPlane:
    """A plane parallel to the X,Z-plane"""
    def __init__(self, z, iorBelow, iorAbove):
        """The z-height, the IOR below this z and the IOR above this z are to be provided.
        IOR must be greater than 1.0"""
        self.z = float(z)
        self.iorBelow = float(iorBelow)
        self.iorAbove = float(iorAbove)

        if self.iorBelow < 1.0 or self.iorAbove < 1.0:
            raise ValueError("IOR must be 1.0 or greater")

    def raysIntersect(self, rays):
        """Calculates the distance along the direction vector from each ray to the plane"""
        dists = (self.z - rays.locs[2]) / rays.dirs[2]
        # If the ray is past the plane there is no intersection
        dists[(dists <= 1e-10) | np.isinf(dists)] = np.NaN
        return dists

    def normals(self, locations, rayDirs):
        """Returns normal vectors on the surface of the plane and the refractive indices

        Arguments:
        location -- unused, for consistency with other shapes
        rayDirs  -- The direction vector of the rays"""
        # Calculate the normal vectors
        zeros = np.zeros(rayDirs.shape[1])
        zDirs = -np.sign(rayDirs[2])
        normals = np.array([zeros, zeros, zDirs])

        # Figure out the refractive indices
        # Is the ray coming from positive side of the z-axis?
        highToLow = zDirs > 0.0  # z direction of the normal
        # First row is incoming side's IOR, second is the IOR on the other side
        iors = np.full((2, highToLow.size), self.iorBelow)
        iors[0,  highToLow] = self.iorAbove
        iors[1, ~highToLow] = self.iorAbove
        return normals, iors


class EmitterDetector:
    """Generates and records rays"""
    def __init__(self, diskCentre, focusDistance, numericalAperture, magnification, sensorRadius, iorBelow, iorAbove):
        """
        Arguments:
        diskCentre        -- Location vector of the centre of the lens disk
        focusDistance     -- Physical distance directly above the disk where the rays converge if they don't intersect any object
        numericalAperture -- NA of the objective. Together with IOR determines the radius of the disk
        magnification     -- Magnification of the lens at the given focal distance
        sensorRadius      -- The radius of the spherical sensor
        iorBelow          -- Refractive index of the medium below the lens
        iorAbove          -- Refractive index of the medium above the lens, used for determining radius and optical path lengths"""
        location = np.asfarray(diskCentre)
        if location.shape != (3,):
            raise ValueError(f"Location must be shaped (3,) but is {location.shape}")
        self.loc = location

        if iorBelow < 1.0:
            raise ValueError(f"IOR below must be 1.0 or greater but it is {iorBelow}")
        if iorAbove < 1.0:
            raise ValueError(f"IOR above must be 1.0 or greater but it is {iorAbove}")
        self.iorBelow = float(iorBelow)
        self.iorAbove = float(iorAbove)

        sinAngle = numericalAperture / self.iorAbove
        self.lensRadius = focusDistance * sinAngle / np.sqrt(1.0 - sinAngle*sinAngle)
        self.sensorRadius = float(sensorRadius)
        self.focusDistance = float(focusDistance)
        self.magnification = float(magnification)
        if self.magnification < 0.0:
            raise ValueError(f"Magnification must be positive but it is {self.magnification}")

        self.opls  = np.empty((0,))
        self.ampls = np.empty((0,))

        self.returnedLocs  = np.empty((2,0))
        self.returnedAmpls = np.empty((0,))

    def raysIntersect(self, rays):
        """Calculates the distance along the direction vector from each ray to the flat lens"""
        dists = (self.loc[2] - rays.locs[2]) / rays.dirs[2]
        # If the ray is past or too far away from the lens there is no intersection
        intersectLocs = rays.locs[:2] + rays.dirs[:2] * dists - self.loc[:2,np.newaxis]  # Relative to self.loc, z == 0 always
        distsFromCentreSq = multinormSq(intersectLocs)
        dists[(dists <= 0.0) | (distsFromCentreSq > self.lensRadius*self.lensRadius)] = np.NaN
        return dists

    def detect(self, rays):
        """Detect rays on a disk-shaped sensor"""
        # Translate projection location such that ray location is at the origin
        projSensorLoc = np.array([[0.0], [0.0], [self.focusDistance]]) + self.loc[:,np.newaxis]
        relLocs = projSensorLoc - rays.locs
        # We start with the distance from the ray (origin) to the sensor plane, along the direction vector. Should be negative.
        dists = relLocs[2] / rays.dirs[2]
        # Find where it should hit the sensor projection
        intersectLocs = rays.locs[:2] + rays.dirs[:2] * dists - projSensorLoc[:2]  # Relative to projection, z == 0 always

        self.returnedLocs = np.hstack((self.returnedLocs, intersectLocs))
        self.returnedAmpls = np.hstack((self.returnedAmpls, rays.ampls))

        # Reject any that miss the projection or are not travelling in the right direction
        distsFromCentreSq = multinormSq(intersectLocs)
        projSensorRadius = self.sensorRadius / self.magnification
        dists[(rays.dirs[2] >= 0.0) | (distsFromCentreSq > projSensorRadius*projSensorRadius)] = np.NaN
        # Adjust IOR to the projected sensor.
        rays.opls += self.iorAbove * dists
        # NOTE The following may be mostly pointless as the OPL difference will be max around a nanometre
        # OPL to lens along OA is constant, no need to add it
        # Rays along this path have the same phase in the principal focus, do not add that distance
        # Only add OPL from principal focus to real sensor
        sensorToFocalPointDist = self.focusDistance * self.magnification * self.magnification / (self.magnification + 1.0)  # TODO This assumes same focal length on either side of lens
        rays.opls += self.iorBelow * np.sqrt(sensorToFocalPointDist*sensorToFocalPointDist + distsFromCentreSq * (self.magnification * self.magnification))
        # Collect valid OPLs and amplitudes
        isDetected = np.isfinite(rays.opls)
        if isDetected.size > 0:
            self.opls = np.hstack((self.opls, rays.opls[isDetected]))
            # self.ampls = np.hstack((self.ampls, rays.ampls[isDetected]))
            self.ampls = np.hstack((self.ampls, rays.ampls[isDetected] * (1 - distsFromCentreSq[isDetected]/(projSensorRadius*projSensorRadius))))  # A smoother falloff

    def generateRays(self, rayCount):
        """Creates a Rays instance with the rays originating from a disk and
        converging on a single point directly above the disk. Optical path lengths
        are all equal where the rays converge."""
        # A golden ratio
        phi = (np.sqrt(5.0)-1.0)/2.0
        radii = np.sqrt(np.linspace(0.0,self.lensRadius*self.lensRadius,rayCount))
        angles = 2.0 * pi * phi * np.arange(rayCount)
        xs = radii * np.cos(angles)
        ys = radii * np.sin(angles)
        diskCentre = np.asfarray(self.loc)[:,np.newaxis]
        origins = diskCentre + np.vstack((xs, ys, np.zeros(rayCount)))
        directions = np.array([[0.0], [0.0], [self.focusDistance]]) + diskCentre - origins  # Not normalised
        opticalPathLengths = -self.iorAbove * multinorm(directions)
        return Rays(origins, directions, opticalPathLengths)

    def readAmplitudeSignal(self, wavenumbers):
        return np.sum(self.ampls * np.exp(1j * np.outer(wavenumbers, self.opls)), axis=1)
