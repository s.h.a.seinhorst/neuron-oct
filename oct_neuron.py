#!/usr/bin/env python
import path_tracing as ptr
import numpy as np
import h5py
pi = np.pi

def renderSpectrum(scene, emitterDetector, wavenumbers, rayCount=3000, maxBounces=11):
    rays = emitterDetector.generateRays(rayCount)
    rays.trace(scene, emitterDetector, maxBounces=maxBounces)
    amplSignal = emitterDetector.readAmplitudeSignal(wavenumbers)
    return ptr.complexNormSq(amplSignal)


def spectraScan(setupFnc, focusFnc, saveFile=None, diameters=None, wavenumbers=None, **kwargs):
    if saveFile and not isinstance(saveFile, (h5py.File, h5py.Group)):
        raise TypeError(f"Save file must either evaluate to False or be HDF5 file or group, but is {type(saveFile)}")

    try:
        wavenumbers = np.asfarray(wavenumbers or np.linspace(2.0*pi/0.900, 2.0*pi/0.650, 1024))
    except ValueError: pass  # NumPy arrays don't support __bool__
    try:
        diameters = np.asfarray(diameters or np.linspace(2.000, 2.020, 31))
    except ValueError: pass  # NumPy arrays don't support __bool__
    spectra = np.empty((diameters.size, wavenumbers.size))

    if saveFile:
        saveFile['wavenumbers'] = wavenumbers
        saveFile['wavenumbers'].make_scale("wavenumbers")

        saveFile['diameters'] = diameters
        saveFile['diameters'].make_scale("diameters")

    for i, diam in enumerate(diameters):
        scene, ed = setupFnc(diam, focusFnc(diam))
        spectra[i] = renderSpectrum(scene, ed, wavenumbers, **kwargs)

        if saveFile:
            group = saveFile.create_group(f"/raw/scan_{i}")
            group.attrs['diameter'] = diam
            group['optical_path_lengths'] = ed.opls
            group['amplitudes']           = ed.ampls

    if saveFile:
        saveFile['spectra'] = spectra
        saveFile['spectra'].dims[0].label = "diameter"
        saveFile['spectra'].dims[1].label = "wavenumber"
        saveFile['spectra'].dims[0].attach_scale(saveFile['diameters'])
        saveFile['spectra'].dims[1].attach_scale(saveFile['wavenumbers'])

    return spectra, wavenumbers, diameters


def readSpectra(filename):
    with h5py.File(filename, "r") as f:
        spectra = f['spectra'][()]
        wavenumbers = f['wavenumbers'][()]
        diameters = f['diameters'][()]

    return spectra, wavenumbers, diameters


def phaseSlope(diams, wavenumbers, sceneFunc, focusFunc, **kwargs):
    """Given an array of diameters, the wavenumbers and a scene generation
    function, calculates the average phase slope over the diameters. Kwargs are
    passed to renderSpectrum"""
    peakIdcs = np.empty_like(diams, dtype=int)
    phases   = np.empty_like(diams, dtype=float)
    for j, diam in enumerate(diams):
        spectrum = renderSpectrum(*sceneFunc(diam, focusFunc(diam)), wavenumbers, **kwargs)
        # Scale from -1 to 1
        max = spectrum.max()
        min = spectrum.min()
        spectrum = (2 * spectrum - (max + min)) / (max - min)
        # Hamming window and subtracting mean
        spectrum *= np.hamming(spectrum.size)
        spectrum -= spectrum.mean()
        # FFT, find peaks and take their phase
        fourier = np.fft.rfft(spectrum)
        peakIdcs[j] = np.argmax(ptr.complexNormSq(fourier))
        phases[j] = np.angle(fourier[peakIdcs[j]])

    # Make phase not wrap after pi radians, find indices where max peak changes, calculate average phase slope
    # TODO It's probably better to calculate slope for each peak (to avoid filtering peak jumps),
    #      then take only phase slopes of the peaks and return that 1D array.
    #      Let caller take the mean.
    phases = np.unwrap(phases * 2) / 2  # TODO Use 'period = pi' when NumPy 1.21.0+ is out
    noJumpMask = np.diff(peakIdcs) == 0.0  # True for each value where the peakIdcs does not change
    return np.mean((np.diff(phases) / np.diff(diams))[noJumpMask])  # The mean change in phase wherever there was no jump


def optimalFocus(diameter):
    """This is a rough approximation of where the lens needs to focus for the rays
    to converge near the centre of the cylinder and return the maximum signal
    when the cylinder is 5nm off the plane"""
    return 0.581*diameter + 0.069


def OCTNeuronScene(
        diam,
        focusZ,
        neuronGlassDistance=0.005,
        focusDistance=300.0,
        numericalAperture=1.2,
        magnification=20,
        sensorRadius=2.5,
        iorGlass=1.52,
        iorWater=1.33,
        iorNeuron=1.39,
        focusYOffset=0.0):
    """Given a size of neuron generates a basic scene that can be rendered"""
    ed = ptr.EmitterDetector(
            [0.0,focusYOffset,-focusDistance+focusZ],
            focusDistance=focusDistance,
            numericalAperture=numericalAperture,
            magnification=magnification,
            sensorRadius=sensorRadius,
            iorBelow=1.00,
            iorAbove=iorGlass)
    plane = ptr.XYPlane(0.0, iorBelow=iorGlass, iorAbove=iorWater)
    cyl = ptr.XCylinder(diam/2.0, [0,diam/2.0+neuronGlassDistance], iorInside=iorNeuron, iorOutside=iorWater)
    return [plane, cyl], ed


def genWavenumbers(shortestWavelength=0.650, longestWavelength=0.900, num=1024, **kwargs):
    return np.linspace(2*pi/longestWavelength, 2*pi/shortestWavelength, num=num, **kwargs)


def processSignalSeries(spectra, wavenumbers, diameters):
    # Stretch from -1 to 1
    maxs = spectra.max(axis=-1)[:,np.newaxis]
    mins = spectra.min(axis=-1)[:,np.newaxis]
    spectra = (2 * spectra - (maxs + mins)) / (maxs - mins)
    # Windowing function
    spectra *= np.hamming(spectra.shape[1])
    # Subtract mean
    spectra -= spectra.mean(axis=-1)[:,np.newaxis]
    # Fourier transform
    fourier = np.fft.rfft(spectra)
    realSpace = np.fft.rfftfreq(wavenumbers.size, (wavenumbers[-1] - wavenumbers[0])/(wavenumbers.size - 1)) * pi
    # Find peaks
    FFTIntensity = ptr.complexNormSq(fourier)
    peakIndex = np.argmax(FFTIntensity, axis=-1)
    # Phase around peaks
    phases = np.unwrap(2 * np.angle(fourier[:,:peakIndex.max()+1]), axis=0) / 2  # TODO use 'period = pi' when using NumPy 1.21.0+
    # Phase slope at peak
    allIndices = np.indices(fourier.shape[:-1])
    phaseSlope = np.diff(phases, axis=0)[allIndices[0,:-1],peakIndex[:-1]]/np.diff(diameters)
    return phaseSlope


def main():
    diameters = np.linspace(1.0, 2.0, 51)
    wavenumbers = genWavenumbers()
    measuredPhaseSlope = phaseSlope(diameters, wavenumbers, OCTNeuronScene, optimalFocus)
    expectedPhaseSlope = OCTNeuronScene(1.0, 1.0)[0][1].iorInside * (wavenumbers[0] + wavenumbers[-1])
    print(f"measured: {measuredPhaseSlope:.2f}, expected: {expectedPhaseSlope:.2f}")


if __name__ == "__main__":
    main()
