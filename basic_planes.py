#!/bin/env python
"""
Simulating light bouncing off a (stack of) plane(s). It is assumed that the
light originates in vacuum (ior=1).

## Standard units
- Length: µm
"""

import copy
from matplotlib import colors, cm
import matplotlib.pyplot as plt
import numpy as np
from multiprocessing import Pool
from functools import partial
pi = np.pi

rng = np.random.default_rng()

class Ray:
    "A single ray of light"
    def __init__(self, location=0.0, amplitude=1.0, up=True, bounces=11):
        self.location = location
        self.direction_up = up
        self.amplitude = amplitude
        self.bounces = bounces
        self.optical_path_length = 0.0

class Interface:
    """A plane where light can reflect and refract.

    Arguments:
    height -- distance between the plane and the origin in µm
    ior    -- index of refraction above this plane
    """

    def __init__(self, height, ior):
        if ior < 1.0:
            raise ValueError(f"Refractive index must be >= 1.0, but received {ior}")

        self.height = height
        self.ior = ior

    def __lt__(self, other):
        return self.height < other.height


def verify_planes_order(planes):
    "Verifies that the planes are ordered by height and no not intersect"
    for p_low, p_high in zip(planes, planes[1:]):
        if p_low > p_high or p_low.ior < 1.0:
            return False
    if planes[-1].ior < 1.0:  # The last plane is not checked in the for-loop
        return False
    return True


def find_ray_intersect(ray, planes):
    """Planes are assumed to be sorted by height"""
    if not planes:
        return None

    low = 0
    high = len(planes) - 1

    if ray.direction_up:
        # Find the first plane in the list where plane.height > ray.location (binary search)
        while low < high:
            i = (high + low) // 2
            if planes[i].height > ray.location:
                high = i
            else:
                low = i + 1
        return low if planes[low].height > ray.location else None

    else: # down
        # Find the last plane in the list where plane.height < ray.location (binary search)
        j = 0
        while low < high:
            i = (high + low + 1) // 2
            if planes[i].height < ray.location:
                low = i
            else:
                high = i - 1
        return high if planes[high].height < ray.location else None


def norm_sq(z):
    return np.real(z * np.conj(z))


def trace_ray(ray, planes, ior=1.0):
    """Ray tracing generator that returns the optical path length and amplitude of each
    ray that returns to the sensor within the ray's bounce limit.

    Arguments:
    ray    -- the ray to be traced
    planes -- an ordered list of the planes that the ray may intersect
    ior    -- the IOR between the current location and the next plane. Mainly for internal use"""
    next_plane_idx = find_ray_intersect(ray, planes)  # Find the plane which the ray will intersect next
    if next_plane_idx is not None:  # The ray intersects with a plane
        next_plane_height = planes[next_plane_idx].height

        ray.optical_path_length += abs(next_plane_height - ray.location) * ior
        ray.location = next_plane_height
        if ray.bounces > 0:
            ray.bounces -= 1

            next_ior_idx = next_plane_idx - (1 if not ray.direction_up else 0)
            next_ior = planes[next_ior_idx].ior if next_ior_idx >= 0 else 1.0
            reflectance = (next_ior - ior) / (next_ior + ior)
            transmittance = 1.0 - reflectance

            # Make a new ray that gets transmitted
            new_ray = copy.deepcopy(ray)
            new_ray.amplitude *= transmittance

            # This ray gets reflected
            ray.direction_up = not ray.direction_up
            ray.amplitude *= reflectance

            yield from trace_ray(    ray, planes,      ior)
            yield from trace_ray(new_ray, planes, next_ior)

    elif not ray.direction_up:  # The ray gets picked up by the sensor
        yield ray.optical_path_length, ray.amplitude


def scan_sheet(thickness, ior=1.5, shortest_wavelength=0.650, longest_wavelength=0.900, resolution=1024, snr=None):
    r = (ior-1.0)/(ior+1.0)
    r2 = r*r

    lowest_wavenumber  = 2 * pi / longest_wavelength
    highest_wavenumber = 2 * pi / shortest_wavelength

    wavenumbers = np.linspace(lowest_wavenumber, highest_wavenumber, resolution)
    x = np.cos(2 * ior * thickness * wavenumbers)
    spectrum = (x - r2)/(1 + r2*r2 - 2*r2*x)

    if snr is not None:
        RMS_signal = np.sqrt(np.mean(spectrum*spectrum))
        RMS_noise = RMS_signal / snr
        spectrum += rng.normal(scale=RMS_noise, size=spectrum.shape)

    fourier = np.fft.rfft(spectrum)
    real_space = np.fft.rfftfreq(resolution, (highest_wavenumber - lowest_wavenumber) / resolution) * pi

    return spectrum, wavenumbers, fourier, real_space


def scan(planes, shortest_wavelength=0.650, longest_wavelength=0.900, resolution=1024, snr=None):
    """Trace a ray across the planes, calculate the spectrum between the given
    wavelengths at the given resolution, and return the spectrum, wavenumbers, FFT
    and real space.

    Arguments:
    planes                      -- ordered list of Interfaces the light will interact with
    shortest_wavelength (0.650) -- shortest wavelength in the light
    longest_wavelength  (0.900) -- longest wavelength in the light
    resolution (1024)           -- number of intermediate wavelengths to consider
    snr (None)                  -- if not None then linear signal to noise ratio"""


    if not verify_planes_order(planes):
        raise ValueError("Invalid set of planes")

    # Since this is a one-dimensional scene, every ray will act the same. We only need to trace one input ray.
    ray = Ray(location = planes[0].height - 1.0)  # TODO Ugly

    lowest_wavenumber  = 2 * pi / longest_wavelength
    highest_wavenumber = 2 * pi / shortest_wavelength

    OPLs, amplitudes = np.array(list(trace_ray(ray, planes))).T
    wavenumbers = np.linspace(lowest_wavenumber, highest_wavenumber, resolution)

    ampl_spectrum = np.sum(amplitudes * np.exp(1j * np.outer(wavenumbers, OPLs)), axis=1)  # The (complex) amplitude of the light at a specific wavelength
    spectrum = norm_sq(ampl_spectrum)  # The light intensity at given wavelength
    spectrum -= np.mean(spectrum)  # Remove the spike in the FFT at x=0

    if snr is not None:  # Apply Gaussian (read) noise
        RMS_signal = np.sqrt(np.mean(spectrum*spectrum))
        RMS_noise = RMS_signal / snr
        spectrum += rng.normal(scale=RMS_noise, size=spectrum.shape)

    fourier = np.fft.rfft(spectrum)
    real_space = np.fft.rfftfreq(resolution, (highest_wavenumber - lowest_wavenumber) / resolution) * pi

    return spectrum, wavenumbers, fourier, real_space


def plot_scan(spectrum, wavenumbers, fourier, real_space, real_xlim=(0,None), expected=None, save_path=None, show=False):
    fig, (ax_spectrum, ax_fft_mag, ax_fft_phase) = plt.subplots(3, figsize=(4.8, 4.8), constrained_layout=True)

    ax_spectrum.plot(wavenumbers, spectrum)
    ax_spectrum.tick_params(left=False, labelleft=False)
    ax_spectrum.set_xlabel(r"Wave number $k=2\pi/\lambda$ ($\mathrm{\mu m}^{-1}$)")
    ax_spectrum.set_ylabel("Intensity $I$")

    peaks = norm_sq(fourier)
    if expected:
        ax_fft_mag.axvline(expected, c='red', ls='--', label="Expected signal")
    ax_fft_mag.plot(real_space, peaks)
    ax_fft_mag.set_xlim(*real_xlim)
    ax_fft_mag.set_ylim(0, None)
    ax_fft_mag.set_ylabel("FFT$^2$")
    ax_fft_mag.tick_params(left=False, labelleft=False, bottom=False, labelbottom=False)
    ax_fft_mag.grid(axis='x')

    angle = np.angle(fourier)
    if expected:
        ax_fft_phase.axvline(expected, c='red', ls='--', label="Expected signal")
    ax_fft_phase.plot(real_space, angle)
    ax_fft_phase.set_xlim(*real_xlim)
    ax_fft_phase.set_ylim(-pi, pi)
    ax_fft_phase.set_xlabel(r"Apparent thickness ($\mathrm{\mu m}$)")
    ax_fft_phase.set_ylabel("FFT phase")
    ax_fft_phase.set_yticks([-pi, -pi/2.0, 0, pi/2.0, pi])
    ax_fft_phase.set_yticklabels([r"$-\pi$", r"$-\pi/2$", "0", r"$\pi/2$", r"$\pi$"])
    ax_fft_phase.grid()

    if save_path:
        fig.savefig(save_path)
    if show:
        plt.show()

    return fig


def correct_jumps(x, threshold=pi):
    if x.ndim == 1:
        jumps = np.hstack(([0], np.diff(x)))
    else:
        jumps = np.vstack((np.zeros((1, *x.shape[1:])), np.diff(x, axis=0)))
    jumps = np.round(jumps / threshold) * threshold
    return x - np.cumsum(jumps, axis=0)


def plot_sub_nm_phase_change(thickness=5.0, max_delta=0.050, steps=51, resolution=1024, save_path=None, show=False):
    deltas = np.linspace(0,max_delta,steps)
    phase_changes = np.empty((deltas.size, 3))
    spectra = np.empty((deltas.size, resolution))
    fouriers = np.empty((deltas.size, resolution//2+1), dtype=complex)

    for i, delta in enumerate(deltas):
        planes = [Interface(0.0, 1.5), Interface(thickness + delta, 1.0)]
        spectra[i], wavenumbers, fouriers[i], real_space = scan(planes, resolution=resolution)

        peak_idx = np.argmax(norm_sq(fouriers[i]))
        fourier = fouriers[i,peak_idx-1:peak_idx+2]
        phase_changes[i] = np.angle(fourier)

    deltas *= 1000  # um to nm

    fig, ((ax_spectrum, ax_at_peak), (ax_fft, ax_near_peak)) = plt.subplots(2,2, figsize=(8.0, 6.4), constrained_layout=True)
    ax_at_peak.plot(deltas, correct_jumps(phase_changes[:,1]))
    ax_at_peak.set_xlim(deltas[0], deltas[-1])
    ax_at_peak.set_xlabel(r"Delta (nm)")
    ax_at_peak.set_ylabel("FFT relative phase at peak")

    ax_near_peak.plot(deltas, correct_jumps(phase_changes[:,0]), label="Before peak")
    ax_near_peak.plot(deltas, correct_jumps(phase_changes[:,2]), label="After peak")
    ax_near_peak.set_xlim(deltas[0], deltas[-1])
    ax_near_peak.set_xlabel(r"Delta (nm)")
    ax_near_peak.set_ylabel("FFT relative phase near peak")
    ax_near_peak.legend()

    ax_spectrum.plot(wavenumbers, spectra.T)
    ax_spectrum.set_xlabel(r"Wave number $k=2\pi/\lambda$ ($\mathrm{\mu m}^{-1}$)")
    ax_spectrum.set_ylabel("Intensity $I$")
    ax_spectrum.set_xlim(wavenumbers[0], wavenumbers[-1])

    lines = ax_fft.plot(real_space, norm_sq(fouriers).T)
    #ax_fft.legend(lines, deltas)
    ax_fft.set_xlim(0, thickness * 1.5 * 1.5)
    ax_fft.set_xlabel(r"Apparent thickness ($\mathrm{\mu m}$)")
    ax_fft.set_ylim(0, None)
    ax_fft.set_ylabel("FFT$^2$")
    ax_fft.tick_params(left=False, labelleft=False)
    ax_fft.grid(axis='x')

    if save_path:
        fig.savefig(save_path)
    if show:
        plt.show()

    return fig


def sub_nm_phase_slope(thickness=5.0, max_delta=0.050, ior=1.5, steps=51, **kwargs):
    deltas = np.linspace(0,max_delta,steps)
    phases = np.empty_like(deltas)
    peak_idxs = np.empty_like(deltas, dtype=int)

    for i, delta in enumerate(deltas):
        planes = [Interface(0.0, ior), Interface(thickness + delta, 1.0)]
        _, _, fourier, real_space = scan(planes, **kwargs)

        peak_idxs[i] = np.argmax(norm_sq(fourier))
        phases[i] = np.angle(fourier[peak_idxs[i]])

    phases = np.unwrap(2 * phases) / 2
    no_jump_mask = np.equal(np.diff(peak_idxs), 0)  # True for each value where the peak_idx does not change
    mean_slope = np.mean((np.diff(phases) / np.diff(deltas))[no_jump_mask])  # The mean change in phase wherever there was no jump
    return mean_slope, phases, deltas


def plot_sub_nm_phase_and_slope_at_different_thicknesses(save_path=None, show=False):
    thicknesses = np.linspace(5, 10, 251)
    ior = 1.5
    steps = 51

    slopes = np.empty_like(thicknesses)
    phases = np.empty((len(thicknesses), steps))
    for i, thickness in enumerate(thicknesses):
       slopes[i], phases[i], deltas = sub_nm_phase_slope(thickness=thickness, ior=ior, max_delta=0.020, steps=steps)


    fig, (ax1, ax2) = plt.subplots(2, figsize=(6.4, 9.6), constrained_layout=True)

    ax1.plot(thicknesses, slopes)
    ax1.set_xlim(thicknesses[0], thicknesses[-1])
    ax1.set_xlabel("True thickness of glass sheet (µm)")
    ax1.set_ylabel("Phase slope (mrad/nm)")

    deltas *= 1000
    lines = ax2.plot(deltas, phases.T)
    # ax2.legend(lines, [f"{np.round(t)} µm" for t in thicknesses], title="True thickness")
    ax2.set_xlim(deltas[0], deltas[-1])
    ax2.set_xlabel("Delta (nm)")
    ax2.set_ylabel("FFT realtive phase at peak")

    if save_path:
        fig.savefig(save_path)
    if show:
        plt.show()

    return fig


def ideal_slope(thickness, ior=1.5, lowest_wavenumber=2*pi/0.900, highest_wavenumber=2*pi/0.650):
    kmax = highest_wavenumber
    kmin = lowest_wavenumber
    diff = kmax - kmin
    sum  = kmax + kmin
    dit = 2 * ior * thickness
    ditdiff =     diff * dit
    qitdiff = 2 * diff * dit
    qitmin = 2 * kmin * dit
    qitmax = 2 * kmax * dit
    sinqitmin = np.sin(qitmin)
    sinqitmax = np.sin(qitmax)
    cosqitdiff = np.cos(qitdiff)
    return ior * (sum*(ditdiff*qitdiff + cosqitdiff - 1) - diff*(np.cos(qitmin) + np.cos(qitmax) + ditdiff*(sinqitmax + sinqitmin))) \
            / (qitdiff*(ditdiff + sinqitmax - sinqitmin) - cosqitdiff + 1)


def ideal_fourier(thickness, real_space, ior=1.5, lowest_wavenumber=2*pi/0.900, highest_wavenumber=2*pi/0.650):
    rsm = real_space - 2 * ior * thickness
    rsp = real_space + 2 * ior * thickness
    return (np.exp(-1j * highest_wavenumber * rsm) - np.exp(-1j * lowest_wavenumber * rsm)) / (-2j * rsm) \
         + (np.exp(-1j * highest_wavenumber * rsp) - np.exp(-1j * lowest_wavenumber * rsp)) / (-2j * rsp)


def plot_sub_nm_phase_slope_at_different_thicknesses(save_path=None, show=False, **kwargs):
    thicknesses = np.linspace(5, 5.25, 101)
    ior = 1.5
    steps = 51
    shortest_wavelength = 0.650
    longest_wavelength  = 0.900

    highest_wavenumber = 2 * pi / shortest_wavelength
    lowest_wavenumber  = 2 * pi / longest_wavelength

    slopes = np.empty_like(thicknesses)
    for i, thickness in enumerate(thicknesses):
        slopes[i], _, _ = sub_nm_phase_slope(thickness=thickness, ior=ior, max_delta=0.050, steps=steps, shortest_wavelength=shortest_wavelength, longest_wavelength=longest_wavelength, **kwargs)


    mean_slope = ior * (lowest_wavenumber + highest_wavenumber)
    plt.plot(thicknesses, ideal_slope(thicknesses, ior=ior, lowest_wavenumber=lowest_wavenumber, highest_wavenumber=highest_wavenumber), label="Algebraic")
    plt.plot(thicknesses, mean_slope +    ior*(3*highest_wavenumber+lowest_wavenumber) / (2*ior*(highest_wavenumber-lowest_wavenumber)*thicknesses - 2), label="Algebraic max bound")
    plt.plot(thicknesses, mean_slope + (2*ior*(3*highest_wavenumber+lowest_wavenumber) * thicknesses + 1) / (4*ior*(highest_wavenumber-lowest_wavenumber)*thicknesses**2 - 4*thicknesses), label="Algebraic max bound")
    plt.plot(thicknesses, slopes, c='tab:red', label="Experiment FFT")
    plt.axhline(mean_slope, c='tab:green', label="Algebraic mean slope")
    plt.xlim(thicknesses[0], thicknesses[-1])
    plt.xlabel("True thickness of glass sheet (µm)")
    plt.ylabel("Phase slope (mrad/nm)")
    plt.legend()

    if save_path:
        plt.savefig(save_path)
    if show:
        plt.show()


def calc_phase_slope_std(snr, max_delta, thicknesses, **kwargs):
    slopes = np.empty_like(thicknesses)
    for i, thickness in enumerate(thicknesses):
        slopes[i], _, _ = sub_nm_phase_slope(thickness=thickness, max_delta=max_delta, snr=snr, **kwargs)

    return np.std(slopes, ddof=1)


def noise_analysis(save_path=None, show=False):
    thicknesses = np.linspace(5, 10, 51)
    ior = 1.5
    steps = 51
    shortest_wavelength = 0.650
    longest_wavelength  = 0.900

    highest_wavenumber = 2 * pi / shortest_wavelength
    lowest_wavenumber  = 2 * pi / longest_wavelength

    max_deltas = [0.01, 0.02, 0.04, 0.08, 0.16]

    slopes = np.empty_like(thicknesses)
    snrs = np.arange(1, 21)
    noise_levels = np.empty((len(snrs), len(max_deltas)))
    snr_delta_combs = np.array(np.meshgrid(snrs, max_deltas)).T.reshape((-1,2))

    cmap = plt.get_cmap('inferno')
    norm = colors.Normalize(vmin=snrs[0], vmax=snrs[-1])
    for snr in snrs:
        _, _, fourier, real_space = scan([Interface(0.0, ior), Interface(5.0, 1.0)], shortest_wavelength=shortest_wavelength, longest_wavelength=longest_wavelength, snr=snr)
        plt.plot(real_space, norm_sq(fourier), c=cmap(norm(snr)))

    for snr in snrs:
        _, _, fourier, real_space = scan([Interface(0.0, ior), Interface(6.6, 1.0)], shortest_wavelength=shortest_wavelength, longest_wavelength=longest_wavelength, snr=snr)
        plt.plot(real_space, norm_sq(fourier), c=cmap(norm(snr)))

    plt.colorbar(cm.ScalarMappable(norm=norm, cmap=cmap))

    plt.xlim(0, 20)
    plt.ylim(0, None)

    """  Noise levels in slope at different thicknesses and SNRs
    slope_kwargs = {"thicknesses":thicknesses, "ior":ior, "steps":steps, "shortest_wavelength":shortest_wavelength, "longest_wavelength":longest_wavelength}
    with Pool() as p:
        noise_levels = p.starmap(partial(calc_phase_slope_std, **slope_kwargs), snr_delta_combs)

    lines = plt.plot(snrs, np.asarray(noise_levels).reshape((len(snrs), -1)))
    plt.legend(lines, [f"{md * 1000} nm" for md in max_deltas], title="Displacement used for\nphase slope calculation")
    plt.xlabel("Signal to noise ratio in light spectrum")
    plt.ylabel("Standard deviation in phase slope (mrad/nm)")
    #"""

    if save_path:
        plt.savefig(save_path)
    if show:
        plt.show()


def noise_histogram(runs=1000, thickness=5.0, ior=1.5, resolution=1024, shortest_wavelength=0.650, longest_wavelength=0.900, snr=None, draw_histograms=True, save_path=None, show=False):
    spectra = np.empty((runs, resolution))
    fouriers = np.empty((runs, resolution//2+1), dtype=complex)
    highest_wavenumber = 2 * pi / shortest_wavelength
    lowest_wavenumber  = 2 * pi / longest_wavelength


    scan_kwargs = {"snr":snr, "shortest_wavelength":shortest_wavelength, "longest_wavelength":longest_wavelength, "resolution":resolution}
    deltas = np.linspace(0,2.5,101)
    std_out = np.empty_like(deltas)
    tops = np.empty((len(deltas), 10))
    bins = np.empty((len(deltas), 11))
    for j, delta in enumerate(deltas):
        planes = [Interface(0.0, ior), Interface(thickness + delta, 1.0)]
        for i in range(runs):
            spectra[i], wavenumbers, fouriers[i], real_space = scan(planes, **scan_kwargs)

        fft2 = norm_sq(fouriers)
        peak_idx = np.argmax(fft2, axis=1)
        phases = np.angle(fouriers[np.arange(fouriers.shape[0]), peak_idx])
        phases = np.unwrap(2 * phases) / 2
        phases -= np.mean(phases)
        measured_shift = phases / (ior * (highest_wavenumber + lowest_wavenumber))
        std_out[j] = measured_shift.std(ddof=1)
        tops[j], bins[j] = np.histogram(measured_shift + thickness + delta, density=True)

    fig, ((ax11, ax12), (ax21, ax22)) = plt.subplots(2,2, figsize=(12.8, 9.6), constrained_layout=True)

    for x in (real_space[:-1]+real_space[1:])/(2*ior):
        ax21.axvline(x, c="lightgray")
        ax12.axvline(x, c="gray")
        ax22.axvline(x, c="gray")

    ax11.plot(wavenumbers, spectra[0])
    ax11.set_xlim(wavenumbers[0], wavenumbers[-1])
    ax11.set_xlabel(r"Wavenumber $k=2\pi/\lambda$ (µm$^{-1}$)")
    ax11.set_ylabel("Light intensity")
    ax11.legend(title=f"SNR = RMS(signal)/RMS(noise) = {snr:.1f} ({10*np.log10(snr):.1f} dB)")
    ax11.set_title(f"Measured spectrum (1 sample at thickness {thickness+delta:.2f} µm)")

    peaks = fft2[np.arange(fft2.shape[0]), peak_idx]
    mean_noise = 10*np.log10(np.mean(fft2[:,peak_idx+100]))
    mean_peak  = 10*np.log10(np.mean(peaks))
    ax21.plot(real_space/ior, 10*np.log10(fft2).T)
    ax21.axhline(mean_noise, c='k')
    ax21.axhline(mean_peak, c='k')
    ax21.legend(title=f"SNR = {mean_peak-mean_noise:.1f} dB")
    ax21.set_xlim(0, 35)
    ax21.set_xlabel("True thickness (µm)")
    ax21.set_ylabel("10 log(FFT$^2$) (dB)")
    ax21.set_title(f"FFTs of measured signals (true thickness {thickness+delta:.2f} µm)")

    thicknesses = np.linspace(thickness+deltas[0], thickness+deltas[-1], 501)
    ax12.plot(thicknesses, ideal_slope(thicknesses, ior=ior, lowest_wavenumber=lowest_wavenumber, highest_wavenumber=highest_wavenumber), c="tab:blue", label="Theoretical ideal")
    ax12.axhline(ior*(highest_wavenumber + lowest_wavenumber), c="tab:orange", label="Theoretical average")
    ax12.legend()
    ax12.sharex(ax22)
    ax12.tick_params(labelbottom=False)
    ax12.set_ylabel("Phase slope (mrad/nm)")
    ax12.set_title("Theoretical phase slope")

    if draw_histograms:
        tops *= std_out.max() / tops.max() * 1000  # Tops as high as std (although it may not always render as such)
        for t, b in zip(tops, bins):
            ax22.bar(b[:-1], t, width=np.diff(b), align='edge')  # The height scale is arbitrary
        ax22.legend(title="Histograms positioned manually,\nheight not to scale")
    else:
        ax22.set_xlim(thickness+deltas[0], thickness+deltas[-1])

    ax22.plot(thickness + deltas, std_out * 1000, c='k', label="Standard deviation")
    ax22.set_ylim(0,None)
    ax22.set_xlabel("True thickness (µm)")
    ax22.set_ylabel("Standard deviation (nm)")
    ax22.set_title("Standard deviation of measured relative thickness")

    if save_path:
        plt.savefig(save_path, dpi=300)
    if show:
        plt.show()

    return fig


def main():
    # plot_sub_nm_phase_change(show=True)
    # plot_sub_nm_phase_slope_at_different_thicknesses(show=True, snr=5.0)
    # plot_sub_nm_phase_and_slope_at_different_thicknesses(show=True)
    # plot_scan(*scan([Interface(0.0, 1.5), Interface(5.0, 1.0)]), real_xlim=(0, 11), expected=7.5, show=True)
    # noise_analysis(show=True)
    noise_histogram(runs=200, thickness=5, show=True, snr=5.0, draw_histograms=False)


if __name__ == "__main__":
    main()
